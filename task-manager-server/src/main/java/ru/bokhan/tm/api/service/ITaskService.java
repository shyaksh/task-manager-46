package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.TaskDTO;
import ru.bokhan.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<TaskDTO, Task> {

    void create(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String name);

    void create(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String name,
            String description
    );

    void add(@Nullable String userId, @Nullable TaskDTO taskDTO);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    @Nullable
    TaskDTO findById(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    TaskDTO findByName(@Nullable String userId, @Nullable String name);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    void removeByName(@Nullable String userId, @Nullable String name);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}