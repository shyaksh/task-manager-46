package ru.bokhan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.AbstractEntityDTO;
import ru.bokhan.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<D extends AbstractEntityDTO, E extends AbstractEntity> {

    @NotNull
    List<D> findAll();

    void clear();

    void load(@Nullable List<D> list);

    void remove(@Nullable E entity);

    void save(@NotNull D entity);

    void remove(@NotNull D entity);

}
