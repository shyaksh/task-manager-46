
package ru.bokhan.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.bokhan.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetServerHost_QNAME = new QName("http://endpoint.tm.bokhan.ru/", "getServerHost");
    private final static QName _GetServerHostResponse_QNAME = new QName("http://endpoint.tm.bokhan.ru/", "getServerHostResponse");
    private final static QName _GetServerPort_QNAME = new QName("http://endpoint.tm.bokhan.ru/", "getServerPort");
    private final static QName _GetServerPortResponse_QNAME = new QName("http://endpoint.tm.bokhan.ru/", "getServerPortResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.bokhan.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetServerHost }
     * 
     */
    public GetServerHost createGetServerHost() {
        return new GetServerHost();
    }

    /**
     * Create an instance of {@link GetServerHostResponse }
     * 
     */
    public GetServerHostResponse createGetServerHostResponse() {
        return new GetServerHostResponse();
    }

    /**
     * Create an instance of {@link GetServerPort }
     * 
     */
    public GetServerPort createGetServerPort() {
        return new GetServerPort();
    }

    /**
     * Create an instance of {@link GetServerPortResponse }
     * 
     */
    public GetServerPortResponse createGetServerPortResponse() {
        return new GetServerPortResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServerHost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bokhan.ru/", name = "getServerHost")
    public JAXBElement<GetServerHost> createGetServerHost(GetServerHost value) {
        return new JAXBElement<GetServerHost>(_GetServerHost_QNAME, GetServerHost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServerHostResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bokhan.ru/", name = "getServerHostResponse")
    public JAXBElement<GetServerHostResponse> createGetServerHostResponse(GetServerHostResponse value) {
        return new JAXBElement<GetServerHostResponse>(_GetServerHostResponse_QNAME, GetServerHostResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServerPort }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bokhan.ru/", name = "getServerPort")
    public JAXBElement<GetServerPort> createGetServerPort(GetServerPort value) {
        return new JAXBElement<GetServerPort>(_GetServerPort_QNAME, GetServerPort.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServerPortResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bokhan.ru/", name = "getServerPortResponse")
    public JAXBElement<GetServerPortResponse> createGetServerPortResponse(GetServerPortResponse value) {
        return new JAXBElement<GetServerPortResponse>(_GetServerPortResponse_QNAME, GetServerPortResponse.class, null, value);
    }

}
