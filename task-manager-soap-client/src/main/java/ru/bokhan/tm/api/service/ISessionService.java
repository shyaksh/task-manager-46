package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISessionService {

    void extractFrom(@NotNull Object port);

    void insertTo(@NotNull Object port);

    void clear();

}
