package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.dto.ProjectDto;
import ru.bokhan.tm.entity.Project;
import ru.bokhan.tm.exception.empty.EmptyDescriptionException;
import ru.bokhan.tm.exception.empty.EmptyIdException;
import ru.bokhan.tm.exception.empty.EmptyNameException;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIndexException;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.entity.ProjectRepository;

import java.util.List;

@Service
public class ProjectService extends AbstractService<ProjectDto, Project> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectDtoRepository dtoRepository;

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        save(project);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        save(project);
    }

    @Override
    public void saveByUserId(@Nullable final String userId, @Nullable final ProjectDto projectDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectDTO == null) throw new EmptyUserIdException();
        projectDTO.setUserId(userId);
        save(projectDTO);
    }

    @NotNull
    @Override
    public List<ProjectDto> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return dtoRepository.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.deleteAllByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectDto findByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public ProjectDto findByUserIdAndIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final List<ProjectDto> list = findAllByUserId(userId);
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Nullable
    @Override
    public ProjectDto findByUserIdAndName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return dtoRepository.findByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void deleteByUserIdAndIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ProjectDto project = findByUserIdAndIndex(userId, index);
        if (project == null) return;
        repository.deleteById(project.getId());
    }

    @Override
    @Transactional
    public void deleteByUserIdAndName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        repository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectDto project = findByUserIdAndId(userId, id);
        if (project == null) throw new IncorrectIdException();
        project.setName(name);
        project.setDescription(description);
        save(project);
    }

    @Override
    @Transactional
    public void updateByUserIdAndIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectDto project = findByUserIdAndIndex(userId, index);
        if (project == null) throw new IncorrectIndexException();
        project.setName(name);
        project.setDescription(description);
        save(project);
    }

    @Override
    public boolean existsByUserIdAndId(@Nullable final String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return dtoRepository.countByUserId(userId);
    }

}