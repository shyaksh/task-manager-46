package ru.bokhan.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.entity.Project;

public interface ProjectRepository extends AbstractRepository<Project> {

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String name);

}