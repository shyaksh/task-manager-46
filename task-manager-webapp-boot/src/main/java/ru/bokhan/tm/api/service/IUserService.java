package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.UserDto;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.RoleType;

public interface IUserService extends IService<UserDto, User> {

    void create(@Nullable String login, @Nullable String password);

    void createWithEmail(@Nullable String login, @Nullable String password, @Nullable String email);

    void createWithRole(
            @Nullable String login,
            @Nullable String password,
            @Nullable RoleType roleType
    );

    @Nullable
    UserDto findById(@Nullable String id);

    @Nullable
    UserDto findByLogin(@Nullable String login);

    void deleteById(@Nullable String id);

    void deleteByLogin(@Nullable String login);

    void updateById(
            @Nullable String id, @Nullable String login,
            @Nullable String firstName, @Nullable String lastName, @Nullable String middleName,
            @Nullable String email
    );

    void updatePasswordById(@Nullable String id, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
