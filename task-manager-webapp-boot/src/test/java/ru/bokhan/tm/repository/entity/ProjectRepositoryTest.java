package ru.bokhan.tm.repository.entity;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.TaskManagerWebapp;
import ru.bokhan.tm.constant.ProjectTestData;
import ru.bokhan.tm.constant.TaskTestData;
import ru.bokhan.tm.constant.UserTestData;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;

import static ru.bokhan.tm.constant.ProjectTestData.PROJECT_LIST;
import static ru.bokhan.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.bokhan.tm.constant.RoleTestData.ROLE_LIST;
import static ru.bokhan.tm.constant.TaskTestData.*;
import static ru.bokhan.tm.constant.UserTestData.USER1;
import static ru.bokhan.tm.constant.UserTestData.USER_LIST;

@Transactional
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskManagerWebapp.class)
public class ProjectRepositoryTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void setUp() {
        UserTestData.initData();
        TaskTestData.initData();
        ProjectTestData.initData();
        USER_LIST.forEach(entityManager::persist);
        ROLE_LIST.forEach(entityManager::persist);
        PROJECT_LIST.forEach(entityManager::persist);
        TASK_LIST.forEach(entityManager::persist);
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void deleteByUserIdAndId() {
        Assert.assertTrue(projectRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertTrue(taskDtoRepository.existsById(USER1_TASK1.getId()));
        Assert.assertTrue(taskDtoRepository.existsById(USER1_TASK2.getId()));

        projectRepository.deleteByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId());

        Assert.assertFalse(projectRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(taskDtoRepository.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskDtoRepository.existsById(USER1_TASK2.getId()));
    }

    @Test
    public void deleteAllByUserId() {
        Assert.assertTrue(projectDtoRepository.findAllByUserId(USER1.getId()).size() > 0);
        Assert.assertTrue(taskDtoRepository.findAllByUserId(USER1.getId()).size() > 0);
        projectRepository.deleteAllByUserId(USER1.getId());
        Assert.assertEquals(Collections.EMPTY_LIST, projectDtoRepository.findAllByUserId(USER1.getId()));
        Assert.assertEquals(Collections.EMPTY_LIST, taskDtoRepository.findAllByUserId(USER1.getId()));
    }

    @Test
    public void deleteByUserIdAndName() {
        Assert.assertTrue(projectRepository.existsById(USER1_PROJECT1.getId()));
        projectRepository.deleteByUserIdAndName(USER1.getId(), USER1_PROJECT1.getName());
        Assert.assertFalse(projectRepository.existsById(USER1_PROJECT1.getId()));
    }

}