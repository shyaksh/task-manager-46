package ru.bokhan.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.UserDto;
import ru.bokhan.tm.util.HashUtil;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static UserDto USER1 = new UserDto();

    @NotNull
    public final static UserDto NEW_USER = new UserDto();

    @NotNull
    public final static UserDto ADMIN1 = new UserDto();

    @NotNull
    public final static List<UserDto> USER_LIST = Arrays.asList(USER1, ADMIN1);

    public static void initData() {
        for (int i = 0; i < USER_LIST.size(); i++) {
            @NotNull final UserDto user = USER_LIST.get(i);
            user.setLogin("user" + i);
            @Nullable final String passwordHash = HashUtil.salt(user.getLogin());
            if (passwordHash != null) user.setPasswordHash(passwordHash);
            user.setEmail("user" + i + "@us.er");
            user.setFirstName("Name" + i);
            user.setLastName("Last-Name" + i);
            user.setMiddleName("Middle-Name" + i);
        }

        NEW_USER.setLogin("NEW_USER");
        @Nullable final String passwordHash = HashUtil.salt(NEW_USER.getLogin());
        if (passwordHash != null) NEW_USER.setPasswordHash(passwordHash);
        NEW_USER.setEmail("new-user@us.er");
        NEW_USER.setFirstName("Name");
        NEW_USER.setLastName("Last-Name");
        NEW_USER.setMiddleName("Middle-Name");
    }

}
