package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.configuration.WebApplicationConfiguration;
import ru.bokhan.tm.constant.ProjectTestData;
import ru.bokhan.tm.constant.TaskTestData;
import ru.bokhan.tm.constant.UserTestData;
import ru.bokhan.tm.dto.UserDto;
import ru.bokhan.tm.entity.Role;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.RoleType;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;
import ru.bokhan.tm.repository.dto.UserDtoRepository;
import ru.bokhan.tm.repository.entity.RoleRepository;
import ru.bokhan.tm.repository.entity.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static ru.bokhan.tm.constant.ProjectTestData.PROJECT_LIST;
import static ru.bokhan.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.bokhan.tm.constant.RoleTestData.ROLE_LIST;
import static ru.bokhan.tm.constant.TaskTestData.*;
import static ru.bokhan.tm.constant.UserTestData.USER1;
import static ru.bokhan.tm.constant.UserTestData.USER_LIST;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class UserDetailsServiceBeanTest {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserDtoRepository userDtoRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Before
    public void setUp() {
        UserTestData.initData();
        TaskTestData.initData();
        ProjectTestData.initData();
        USER_LIST.forEach(entityManager::persist);
        ROLE_LIST.forEach(entityManager::persist);
        PROJECT_LIST.forEach(entityManager::persist);
        TASK_LIST.forEach(entityManager::persist);
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void initUsers() {
        @Nullable final UserDto user = userDtoRepository.findByLogin("test");
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());

        @Nullable final UserDto admin = userDtoRepository.findByLogin("admin");
        Assert.assertNotNull(admin);
        Assert.assertEquals("admin", admin.getLogin());

    }

    @Test
    public void create() {
        @NotNull final String login = "new-user";
        @NotNull final String password = "new-user";
        Assert.assertNull(userDtoRepository.findByLogin(login));

        userService.create(login, password);
        @Nullable final UserDto created = userDtoRepository.findByLogin(login);
        Assert.assertNotNull(created);
        Assert.assertEquals(login, created.getLogin());
    }

    @Test
    public void testCreateWithEmail() {
        @NotNull final String login = "new-user";
        @NotNull final String password = "new-user";
        @NotNull final String email = "new@us.er";

        Assert.assertNull(userDtoRepository.findByLogin(login));

        userService.createWithEmail(login, password, email);
        @Nullable final UserDto created = userDtoRepository.findByLogin(login);
        Assert.assertNotNull(created);
        Assert.assertEquals(login, created.getLogin());
        Assert.assertEquals(email, created.getEmail());
    }

    @Test
    public void createWithRole() {
        @NotNull final String login = "new-user";
        @NotNull final String password = "new-user";
        Assert.assertNull(userDtoRepository.findByLogin(login));

        userService.createWithRole(login, password, RoleType.ADMINISTRATOR);
        @Nullable final User created = userRepository.findByLogin(login);
        Assert.assertNotNull(created);
        Assert.assertEquals(login, created.getLogin());

        @NotNull final List<Role> roles = created.getRoles();
        @Nullable Role roleAdmin = null;
        for (@NotNull final Role role : roles) {
            if (role.getRoleType().equals(RoleType.ADMINISTRATOR)) roleAdmin = role;
        }
        Assert.assertNotNull(roleAdmin);
    }

    @Test
    public void findById() {
        assertReflectionEquals(USER1, userService.findById(USER1.getId()));
    }

    @Test
    public void findByLogin() {
        assertReflectionEquals(USER1, userService.findByLogin(USER1.getLogin()));
    }

    @Test
    public void deleteById() {
        @Nullable final User user = userRepository.findByLogin(USER1.getLogin());
        Assert.assertNotNull(user);
        @NotNull final List<Role> roles = user.getRoles();
        roles.forEach(role -> Assert.assertTrue(roleRepository.existsById(role.getId())));

        Assert.assertTrue(projectDtoRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertTrue(taskDtoRepository.existsById(USER1_TASK1.getId()));
        Assert.assertTrue(taskDtoRepository.existsById(USER1_TASK2.getId()));

        userService.deleteById(USER1.getId());

        Assert.assertFalse(userRepository.existsById(USER1.getId()));
        Assert.assertFalse(projectDtoRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(taskDtoRepository.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskDtoRepository.existsById(USER1_TASK2.getId()));
        roles.forEach(role -> Assert.assertFalse(roleRepository.existsById(role.getId())));
    }

}