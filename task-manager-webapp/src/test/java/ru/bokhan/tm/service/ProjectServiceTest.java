package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.configuration.WebApplicationConfiguration;
import ru.bokhan.tm.constant.ProjectTestData;
import ru.bokhan.tm.constant.TaskTestData;
import ru.bokhan.tm.constant.UserTestData;
import ru.bokhan.tm.dto.ProjectDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static ru.bokhan.tm.constant.ProjectTestData.*;
import static ru.bokhan.tm.constant.RoleTestData.NEW_USER_ROLE;
import static ru.bokhan.tm.constant.RoleTestData.ROLE_LIST;
import static ru.bokhan.tm.constant.TaskTestData.TASK_LIST;
import static ru.bokhan.tm.constant.UserTestData.*;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class ProjectServiceTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void setUp() {
        UserTestData.initData();
        TaskTestData.initData();
        ProjectTestData.initData();
        USER_LIST.forEach(entityManager::persist);
        ROLE_LIST.forEach(entityManager::persist);
        PROJECT_LIST.forEach(entityManager::persist);
        TASK_LIST.forEach(entityManager::persist);
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void findAll() {
        @NotNull final List<ProjectDto> list = projectService.findAll();
        Assert.assertEquals(PROJECT_LIST.size(), list.size());
    }

    @Test
    public void clear() {
        projectService.clear();
        Assert.assertEquals(Collections.EMPTY_LIST, projectService.findAll());
    }

    @Test
    public void load() {
        // TODO: 03.03.2021
    }

    @Test
    public void delete() {
        Assert.assertTrue(projectService.existsByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId()));
        projectService.delete(USER1_PROJECT1);
        Assert.assertFalse(projectService.existsByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void save() {
        Assert.assertFalse(projectService.existsByUserIdAndId(NEW_USER.getId(), NEW_USER_PROJECT1.getId()));
        entityManager.persist(NEW_USER);
        entityManager.persist(NEW_USER_ROLE);
        projectService.save(NEW_USER_PROJECT1);
        Assert.assertTrue(projectService.existsByUserIdAndId(NEW_USER.getId(), NEW_USER_PROJECT1.getId()));
    }

    @Test
    public void create() {
        @NotNull final ProjectDto expected = new ProjectDto();
        @NotNull final String projectName = "new";
        expected.setName(projectName);
        expected.setUserId(USER1.getId());
        projectService.create(USER1.getId(), projectName);
        @Nullable final ProjectDto actual = projectService.findByUserIdAndName(USER1.getId(), projectName);
        Assert.assertNotNull(actual);
        expected.setId(actual.getId());
        assertReflectionEquals(expected, actual);
    }

    @Test
    public void createWithDescription() {
        @NotNull final ProjectDto expected = new ProjectDto();
        @NotNull final String projectName = "new";
        @NotNull final String projectDescription = "new-project";
        expected.setName(projectName);
        expected.setDescription(projectDescription);
        expected.setUserId(USER1.getId());

        projectService.create(USER1.getId(), projectName, projectDescription);
        @Nullable final ProjectDto actual = projectService.findByUserIdAndName(USER1.getId(), projectName);
        Assert.assertNotNull(actual);
        expected.setId(actual.getId());
        assertReflectionEquals(expected, actual);
    }

    @Test
    public void saveByUserId() {
        Assert.assertFalse(projectService.existsByUserIdAndId(NEW_USER.getId(), NEW_USER_PROJECT1.getId()));
        entityManager.persist(NEW_USER);
        entityManager.persist(NEW_USER_ROLE);
        projectService.saveByUserId(NEW_USER.getId(), NEW_USER_PROJECT1);
        Assert.assertTrue(projectService.existsByUserIdAndId(NEW_USER.getId(), NEW_USER_PROJECT1.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final Set<ProjectDto> actual = new HashSet<>(projectService.findAllByUserId(USER1.getId()));
        assertReflectionEquals(new HashSet<>(USER1_PROJECT_LIST), actual);
    }

    @Test
    public void deleteAllByUserId() {
        Assert.assertTrue(projectService.findAllByUserId(USER1.getId()).size() > 0);
        Assert.assertTrue(taskService.findAllByUserId(USER1.getId()).size() > 0);
        projectService.deleteAllByUserId(USER1.getId());
        Assert.assertEquals(Collections.EMPTY_LIST, projectService.findAllByUserId(USER1.getId()));
        Assert.assertEquals(Collections.EMPTY_LIST, taskService.findAllByUserId(USER1.getId()));
    }

    @Test
    public void findByUserIdAndId() {
        assertReflectionEquals(
                USER1_PROJECT1,
                projectService.findByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId())
        );
    }

    @Test
    public void findByUserIdAndIndex() {
        assertReflectionEquals(
                projectService.findAllByUserId(USER1.getId()).get(1),
                projectService.findByUserIdAndIndex(USER1.getId(), 1)
        );
    }

    @Test
    public void findByUserIdAndName() {
        assertReflectionEquals(
                USER1_PROJECT1,
                projectService.findByUserIdAndName(USER1.getId(), USER1_PROJECT1.getName())
        );
    }

    @Test
    public void deleteByUserIdAndId() {
        Assert.assertTrue(projectService.existsByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId()));
        projectService.deleteByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertFalse(projectService.existsByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void deleteByUserIdAndIndex() {
        @Nullable final ProjectDto second = projectService.findByUserIdAndIndex(USER1.getId(), 1);
        Assert.assertNotNull(second);
        projectService.deleteByUserIdAndIndex(USER1.getId(), 1);
        Assert.assertFalse(projectService.existsByUserIdAndId(USER1.getId(), second.getId()));
    }

    @Test
    public void deleteByUserIdAndName() {
        Assert.assertTrue(projectService.existsByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId()));
        projectService.deleteByUserIdAndName(USER1.getId(), USER1_PROJECT1.getName());
        Assert.assertFalse(projectService.existsByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void updateById() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newName";
        projectService.updateById(
                USER1.getId(),
                USER1_PROJECT1.getId(),
                newName,
                newDescription
        );
        @Nullable final ProjectDto actual =
                projectService.findByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.getName(), newName);
        Assert.assertEquals(actual.getDescription(), newDescription);
    }

    @Test
    public void updateByUserIdAndIndex() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newName";
        projectService.updateByUserIdAndIndex(
                USER1.getId(),
                1,
                newName,
                newDescription
        );
        @Nullable final ProjectDto actual =
                projectService.findByUserIdAndIndex(USER1.getId(), 1);
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.getName(), newName);
        Assert.assertEquals(actual.getDescription(), newDescription);
    }

    @Test
    public void existsByUserIdAndId() {
        Assert.assertFalse(projectService.existsByUserIdAndId(ADMIN1.getId(), USER1_PROJECT1.getId()));
        Assert.assertTrue(projectService.existsByUserIdAndId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void countByUserId() {
        Assert.assertEquals(projectService.countByUserId(USER1.getId()), USER1_PROJECT_LIST.size());
    }

}