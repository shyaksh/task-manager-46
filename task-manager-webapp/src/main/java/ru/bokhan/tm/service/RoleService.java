package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bokhan.tm.api.service.IRoleService;
import ru.bokhan.tm.dto.RoleDto;
import ru.bokhan.tm.entity.Role;
import ru.bokhan.tm.enumerated.RoleType;
import ru.bokhan.tm.exception.empty.EmptyIdException;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.repository.dto.RoleDtoRepository;

@Service
public class RoleService extends AbstractService<RoleDto, Role> implements IRoleService {

    @Autowired
    private RoleDtoRepository roleDtoRepository;

    @Override
    public RoleDto createWithoutSaving(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final RoleDto role = new RoleDto(userId);
        return role;
    }

    @Override
    public RoleDto createByUserIdAndTypeWithoutSaving(
            @Nullable final String userId,
            @Nullable final RoleType roleType
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final RoleDto role = new RoleDto(userId);
        if (roleType != null) role.setRoleType(roleType);
        return role;
    }

    @Nullable
    @Override
    public RoleDto findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return roleDtoRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        roleDtoRepository.deleteById(id);
    }

}
