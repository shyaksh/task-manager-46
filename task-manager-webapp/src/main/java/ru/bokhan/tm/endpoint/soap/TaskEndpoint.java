package ru.bokhan.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;
import ru.bokhan.tm.util.SecurityUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    @WebMethod
    public List<TaskDto> findTaskAll() {
        return taskDtoRepository.findAllByUserId(SecurityUtil.getUserId());
    }

    @NotNull
    @WebMethod
    public TaskDto saveTask(@NotNull @WebParam(name = "task") final TaskDto task) {
        task.setUserId(SecurityUtil.getUserId());
        return taskDtoRepository.save(task);
    }

    @Nullable
    @WebMethod
    public TaskDto findTaskById(@NotNull @WebParam(name = "id") final String id) {
        return taskDtoRepository.findByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @WebMethod
    public boolean existsTaskById(@NotNull @WebParam(name = "id") final String id) {
        return taskDtoRepository.existsByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @WebMethod
    public long countTask() {
        return taskDtoRepository.countByUserId(SecurityUtil.getUserId());
    }

    @WebMethod
    public void deleteTaskById(@NotNull @WebParam(name = "id") final String id) {
        taskDtoRepository.deleteByUserIdAndId(SecurityUtil.getUserId(), id);
    }

    @WebMethod
    public void deleteTaskAll() {
        taskDtoRepository.deleteAllByUserId(SecurityUtil.getUserId());
    }

}
